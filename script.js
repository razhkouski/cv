class Block {
    constructor(className, html) {
        this.className = className;
        this.html = html;
    }

    render() {
        const element = document.querySelector(this.className);
        if (element) element.innerHTML = this.html;
    }
}

class Button extends Block {
    constructor(className, html, parentElClassName) {
        super(className, html);
        this.parentElClassName = parentElClassName;
    }

    renderButton() {
        const element = document.querySelector(this.parentElClassName);
        const button = document.createElement("button");
        if (element) {
            button.innerHTML = this.html;
            button.classList.add(this.className);
            element.append(button);
        }
    }
}

class ThemeButton extends Button {
    constructor(className, html, parentElClassName) {
        super(className, html, parentElClassName);
    }

    changeTheme(tagOrClass) {
        const changeThemeButton = document.querySelector(`.${this.className}`);
        console.log(`.${this.className}`);   
        if (changeThemeButton) {
            changeThemeButton.addEventListener("click", () => {
                document.querySelector(tagOrClass).classList.toggle("background-theme-default");
                document.querySelector(tagOrClass).classList.toggle("background-theme-dark");
            });
        }
    }
}
class EditButton extends Button {
    constructor(className, html, parentElClassName) {
        super(className, html, parentElClassName);
    }

    editBlock(editableBlockSelector) {
        const editButton = document.querySelector(`.${this.className}`);
        const editableBlock = document.querySelector(editableBlockSelector);
        editButton.addEventListener("click", () => {
            if (editButton.textContent === "Редактировать") {
                editButton.textContent = "Сохранить";
                editableBlock.contentEditable = true;
                editableBlock.focus();
            } else {
                editButton.textContent = "Редактировать";
                editableBlock.contentEditable = false;
            }
        });
    }
}

class Image extends Block {
    constructor(className, src, width = "250", height = "250", alt = "") {
        super(className);
        this.src = src;
        this.width = width;
        this.height = height;
        this.alt = alt;
    }

    renderImage() {
        const parentBlock = document.querySelector(this.className);
        const image = document.createElement("img");
        image.src = this.src;
        image.width = this.width;
        image.height = this.height;
        image.alt = this.alt;
        parentBlock.append(image);
    }
}

const infoBlock = new Block(
    ".info",
    `<h1>Ilya Razhkouski</h1>
    <h3>Programmer</h3>
    <h5>Minsk, Belarus</h5>
    <br>
    <ul>
      <li>i.razhkouski@gmail.com</li>
      <li>+375(29)260-68-47</li>
      <li><a href="http://t.me/poiskull" target="_blank">Telegram</a></li>
    </ul>
    <br>`
);
infoBlock.render();

const aboutMeBlock = new Block(
    ".about-myself",
    `<h2>About Me</h2>
    <p>I'm a 23 year old programmer from Minsk, Belarus. I'm a highly motivated young specialist looking for opportunities for personal growth. 
    Showing superb analytical skills, attention to details and adaptability, as well as high motivation, decent communication skills (my B1 English proficiency level also helps me with getting intercultural communication experience) and initiative.</p>
    <p class="university">In 2015 I entered Belarusian State University of Informatics and Radioelectronics where I studied at the Faculty of Computer-Aided Design and received my major in Business Management in July, 2019.</p>`
);
aboutMeBlock.render();

const skillsDevelopmentBlock = new Block(
    ".skills-development",
    `<h4>Development</h4>
    <ul>
    <li>HTML5/CSS3</li>
    <li>JavaScript</li>
    <li>SQL Databases</li>
    <li>C#</li>
    </ul>`    
);
skillsDevelopmentBlock.render();

const skillsSoftwareBlock = new Block(
    ".skills-software",
    `<h4>Software</h4>
    <ul class="software">
    <li>Visual Studio Code</li>
    <li>MS Visual Studio</li>
    <li>MS SQL Server Management Studio</li>
    <li>ERwin Data Modeler</li>
    </ul>`
);
skillsSoftwareBlock.render();

const educationBlock = new Block(
    ".education",
    `<h2>Education</h2>
    <p>Gymnasium №17, Minsk graduate of 2013.</p>
    <p>Secondary school № 4, Minsk graduate of 2015.</p>
    <p>Belarusian State University of Informatics and Radioelectronics graduate of 2019</p>`
);
educationBlock.render();

const editSoftwareSkillsButton = new EditButton(
    "edit-skills-software",
    "Редактировать",
    ".skills-software"
);
editSoftwareSkillsButton.renderButton();
editSoftwareSkillsButton.editBlock(".software");

const editAboutMeButton = new EditButton(
    "edit-about-me",
    "Редактировать",
    ".about-myself"
);
editAboutMeButton.renderButton();
editAboutMeButton.editBlock(".university");

const changeThemeButton = new ThemeButton("change-theme-button", "Сменить тему", ".info");
changeThemeButton.renderButton();
changeThemeButton.changeTheme("body");

const photo = new Image(".photo", "img/ilya-razhkouski.jpg", 250, 250, "Ilya Razhkouski. Photo for CV");
photo.renderImage();